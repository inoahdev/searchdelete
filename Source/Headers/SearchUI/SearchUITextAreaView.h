//
//  Source/Headers/SearchUI/SearchUITextAreaView.h
//
//  Created by inoahdev on 12/25/16
//  Copyright © 2016 inoahdev. All rights reserved.
//

#ifndef SEARCHUI_SEARCH_UI_TEXT_AREA_VIEW_H
#define SEARCHUI_SEARCH_UI_TEXT_AREA_VIEW_H

#import <CoreFoundation/CoreFoundtion.h>

@interface SearchUITextAreaView : UIView
@property (retain) UILabel *titleLabel;
@end

#endif
