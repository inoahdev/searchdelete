//
//  Source/Headers/_SBAlertController.h
//
//  Created by inoahdev on 12/26/16
//  Copyright © 2016 inoahdev. All rights reserved.
//

#ifndef SPRINGBOARDUI_SBALERTCONTROLLER_H
#define SPRINGBOARDUI_SBALERTCONTROLLER_H

#import <UIKit/UIKit.h>
#import "SBAlertItem.h"

@interface _SBAlertController : UIAlertController
- (SBAlertItem *)alertItem;
@end

#endif
